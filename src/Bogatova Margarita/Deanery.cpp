﻿#include "Deanery.h"

    Deanery:: Deanery()
	{
		numSt = 0;
		numGr = 0;
	}
  	Deanery::~Deanery()
	{
		for (int i = 0; i < numSt; i++)
			delete st[i];
		delete st;
		for (int i = 0; i < numGr; i++)
			delete gr[i];
		delete gr;
	}
    int  Deanery::loadGroups(string fileName)
	{
		ifstream file(fileName, ios_base::in);
		if (!file.is_open()) // если файл небыл открыт
		{
			cout << "Файл group не может быть открыт\n";
			return 1;
		}
		char  str[256];
		while (!file.eof())
		{
			int i = 0, temp = 0;
			file.getline(str, 256);
			while (str[i] >= '0' && str[i] <= '9')
			{
				temp = temp * 10 + str[i++] - '0';
			}
			if (numGr == 0)
			{
				gr = new Group*[numGr + 1];
				gr[numGr] = new Group(temp);
			}
			else {
				Group **copy = new Group*[numGr + 1];
				for (int i = 0; i < numGr; i++)
				{
					copy[i] = gr[i];
				}
				delete[] gr;
				gr = copy;
				gr[numGr] = new Group(temp);
			}
			++numGr;
		}
		file.close();
		return 0;
	}
	int  Deanery::loadStudent(string fileName)
	{
		ifstream file(fileName, ios_base::in);
		if (!file.is_open()) // если файл не был открыт
		{
			cout << "Файл  Student не может быть открыт \n";
			return 1;
		}
		char  str[256];
		while (!file.eof())
		{
			int tempID = 0;
			int tempTitle = 0;
			string tempfio = "";
			int tempMark = 0;
			char temp[50] = { 0 };
			int i = 0, j;
			file.getline(str, 256);
			while (str[i] >= '0' && str[i] <= '9')
			{
				tempID = tempID * 10 + str[i++] - '0';
			} // считываем ИД
			i++;  j = 0;
			// Считываем ФИО
			while ((str[i] <= 'я' && str[i] >= 'А') || str[i] == ' ')
			{
				temp[j++] = str[i++];
			}
			tempfio = temp;
			while (str[i] != ' ') //Считываем номер группы
			{
				tempTitle = tempTitle * 10 + str[i++] - '0';
			}
			//выделяем память под студента
			if (numSt == 0)
			{
				st = new Student*[numSt + 1];
				st[numSt] = new Student(tempfio, tempID);
			}
			else
			{
				Student **copy = new Student*[numSt + 1];
				for (int j = 0; j < numSt; j++)
				{
					copy[j] = st[j];
				}
				delete[] st;
				st = copy;
				st[numSt] = new Student(tempfio, tempID);
			}
			//ищем соответствующую группу, вызываем addStudent
			bool flag = false;
			for (j = 0; j < numGr; j++)
			{
				int num = gr[j]->getTitle();
				if (num == tempTitle)
				{
					gr[j]->addStudent(st[numSt]);
					flag = true;
					break;
				}
			}
			if (flag == false)
				cout << "Студент "<< tempID << tempfio <<"не может быть добавлен ни в одну из имеющихся групп\n";

			//Добавляем оценки
			i++;
			while (str[i] >= '0' && str[i] <= '9')
			{
				tempMark = str[i++] - '0';
				st[numSt]->addMark(tempMark);
			}
			++numSt;
		}
		file.close();
		return 0;
	}
	int  Deanery::findGroup(int id)
	{
		int i; bool flag = false;
		for (i = 0; i < numSt; i++)
			if (st[i]->getId() == id)
			{
				flag = true;
				return st[i]->getGr()->getTitle();
				break;
			}
		if (flag == false)
		{
			cout << "Данный студент отсутствует в базе";
		}
	}
	void Deanery::findExcellentSt(int TitleGr)
	{
		int i;
		for (i = 0; i < numSt; i++)
		{
			if (TitleGr == st[i]->getGr()->getTitle())
				if (st[i]->getAvMark() > 4.5)
				{
					st[i]->PrintFio();
					cout << " - отличник" << endl;
				}
		}
	}
	void Deanery::PrintAvMarkGroup()
	{
		int i;
		for (i = 0; i < numGr; i++)
		{
			cout << "Средний бал группы ";
			cout <<  gr[i]->getTitle()<<" "<< gr[i]->getAvMark()  << endl;
		}
	}
	void Deanery::setHead()
	{
		int i;
		for (i = 0; i < numGr; i++)
			gr[i]->setHead();
	}
	void Deanery::PrintHead(int TitleGr)
	{
		int i;
		for (i = 0; i < numGr; i++)
		{
			if (TitleGr == gr[i]->getTitle())
			{
				gr[i]->getHead()->PrintFio();
				cout << " - староста группы " << TitleGr << endl;
			}
		}
	}
	void Deanery::AddRandMark(int id)
	{
		srand(time(NULL));
		int i;
		for (i = 0; i < numSt; i++)
		{
			if (st[i]->getId() == id)
			{
				int randMark = rand() % 4 + 2;
				st[i]->addMark(randMark);
			}
		}
	}
	void Deanery::DeleteBadStudent()
	{
		int i;
		for (i = 0; i < numSt; i++)
		{
			if (st[i]->getAvMark() < 3.0)
			{
				st[i]->PrintFio();
				cout << "отчислен(а)"<<endl;
				int tempid = st[i]->getId();
				st[i]->getGr()->RemoveStudent(tempid);
				st[i] = st[numSt-1];
				numSt--;
			}
		}
	}
	void Deanery::TransferStudent(int id, int TitleGr)
	{
		int i,j;
		for (i = 0; i < numSt; i++)
		{
			if (st[i]->getId() == id)
			{
				st[i]->getGr()->RemoveStudent(id);
				break;
			}
		}
		for (j = 0; j < numGr; j++)
			if (TitleGr == gr[j]->getTitle())
			{
				gr[j]->addStudent(st[i]);
			}
		cout << st[i]->getFio() << " переведен(а) в группу " << st[i]->getGr()->getTitle() << endl;
	}
	void Deanery::findBestStudent()
	{
		float max = 0; int NumBestStudent=0;
		for (int i = 0; i < numSt; i++)
		{
			if (st[i]->getAvMark() > max)
			{
				max = st[i]->getAvMark();
				NumBestStudent = i;
			}
		}
		st[NumBestStudent]->PrintFio();
		cout << " - Лучший студент" << endl;
	}
	void Deanery::findBestGroup()
	{
		float max = 0; int NumBestGroup = 0;
		for (int i = 0; i < numGr; i++)
		{
			if (st[i]->getAvMark() > max)
			{
				max = gr[i]->getAvMark();
				NumBestGroup = i;
			}
		}
		cout << gr[NumBestGroup]->getTitle()<<" - лучшая группа" << endl;
	}
	void Deanery::PrintStudent(int id)
	{
		for (int i = 0; i < numSt; i++)
			if (st[i]->getId() == id)
			{
				cout << " Информация о студенте: ";
			    st[i]->PrintFio();
				cout << endl;
				cout<< "ID: "<<st[i]->getId()<<endl;
				cout << "Группа: " << st[i]->getGr()->getTitle()<<endl;
				cout << "Оценки: ";
				st[i]->PrintMark();
				cout << "Средняя оценка: " << st[i]->getAvMark() << endl;;
			}
	}
	 int Deanery::SaveFile(string fileName)
	{
		ofstream file(fileName, ios_base::out);
		if (!file.is_open()) // если файл не был открыт
		{
			cout << "Файл  Student не может быть открыт \n";
			return 1;
		}
		int* tempMark; 
		int numTempMark;
		int i;
		for (i = 0; i < numSt; i++)
		{
			file << st[i]->getId()<< " ";
			file << st[i]->getFio();
			file << st[i]->getGr()->getTitle()<< " ";
			tempMark = st[i]->getMark();
			numTempMark = st[i]->getNumMark();
			for (int j = 0; j < numTempMark; j++)
			{
				file << tempMark[j];
			}

			if (i != numSt - 1)	file << endl;
		}
		file.close();
		return 0;
	}
