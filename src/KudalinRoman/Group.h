#pragma once
#include <iostream>
#include <string>
#include <ctime>
#include "Student.h"
using namespace std;

class Group
{
private:
	string title;
	int studentsNumber;
	Student **stdnt;
	Student *head;
public:
	Group() : title(""), studentsNumber(0), stdnt(nullptr),
		head(nullptr) {};

	explicit Group(string title)
	{
		this->title = title;
		studentsNumber = 0;
		stdnt = nullptr;
		head = nullptr;
	}

	Group(const Group &gr)
	{
		title = gr.title;
		studentsNumber = gr.studentsNumber;
		if (studentsNumber)
		{
			stdnt = new Student*[studentsNumber];
			for (int i = 0; i < studentsNumber; i++)
				stdnt[i] = gr.stdnt[i];
			head = gr.head;
		}
	}

	~Group()
	{
		delete[] stdnt;
		stdnt = nullptr;
	}

	void setTitle(string title) { this->title = title; }
	string getTitle() const { return title; }

	// ���������� �������� � ������.
	void addStudent(Student *s)
	{
		if (!studentsNumber)
		{
			stdnt = new Student*[1];
			stdnt[studentsNumber] = s;
		}
		else
		{
			Student **temp = new Student*[studentsNumber + 1];
			for (int i = 0; i < studentsNumber; i++)
				temp[i] = stdnt[i];
			delete[] stdnt;
			stdnt = temp;
			stdnt[studentsNumber] = s;
		}
		s->setGroup(this);
		studentsNumber++;
	}

	// ����� �������� � ������.
	Student* findStudent(int id)
	{
		int i = 0;
		if (!studentsNumber)
		{
			//cout << "There are no students in this group (" << title << ")" << endl;	// �����, ���� � ��������� ����� �����
																						// ������� � �������������.
			return nullptr;
		}
		else
		{
			for (i = 0; i < studentsNumber; i++)
				if (stdnt[i]->getID() == id)
					return stdnt[i];
			if (i == studentsNumber)
			{
				// cout << "There is no student with this ID in this group (" << title << ")" << endl;	// �����, ���� � ��������� ����� �����
																										// ������� � �������������.
				return nullptr;
			}
		}
	}

	// �������� �������� �� ������.
	void removeStudent(int id)
	{
		int i = 0;
		for (i; i < studentsNumber; i++)
			if (stdnt[i]->getID() == id)
			{
				if (head == stdnt[i])
					head = nullptr;
				if (stdnt[i] != stdnt[studentsNumber - 1])
					stdnt[i] = stdnt[studentsNumber - 1];
				stdnt[studentsNumber - 1] = nullptr;
				studentsNumber--;
				break;
			}

		// �����, ���� � ��������� ����� ����� ������� � �������������.
		//if (i == studentsNumber)
			//cout << "There is no student with this ID in this group (" << title << ")" << endl;
	}

	// ���������� ��������.
	void setHead(int id)
	{
		head = findStudent(id);
	}

	// ��������� ���������� �������� � ������.
	void randomHead()
	{
		if (studentsNumber)
			head = stdnt[rand() % studentsNumber];
	}

	// ��������� ���������� � �������� ������.
	Student* getHead()
	{
		if (!head)
		{
			cout << "The headman hasn't been chosen" << endl;
			return nullptr;
		}
		else
			return head;
	}

	// ������� ������ ��������� ������.
	double getAvgMark()
	{
		double avgMark = 0.;
		if (!studentsNumber)
		{
			cout << "There are no students in this group  (" << title << ")" << endl;
			return 0;
		}
		else
		{
			for (int i = 0; i < studentsNumber; i++)
				avgMark += stdnt[i]->getAvgMark();
			avgMark /= studentsNumber;
			return avgMark;
		}
	}

	// ����� ���������� � ������.
	void findHonoursPupils()
	{
		if (!studentsNumber)
		{
			cout << "There are no students in this group (" << title << ")" << endl;
			return;
		}
		else
		{
			bool isExist = false;	// ����������-����, ���������������
									// � ������� (��� ����������) ����������
									// � ������.

			cout << "\nHonours pupils:" << endl;
			for (int i = 0; i < studentsNumber; i++) 
				if (stdnt[i]->getAvgMark() == 5)
				{
					isExist = true;
					cout << stdnt[i]->getID() << " " << stdnt[i]->getFIO() << endl;
				}
			if (!isExist)
				cout << "There are no honours pupils in this group (" << title << ")" << endl;
		}
	}

	// ����� ���������� � ������.
	void showGroupInfo(ostream& stream)
	{
		stream << "\n**************************************\n" << title << "\n" << endl;
		if (!studentsNumber)
		{
			stream << "There are no students in this group" << endl;
			stream << "**************************************" << endl;
			return;
		}
		else
		{
			stream << "Students:" << endl;
			for (int i = 0; i < studentsNumber; i++)
			{	
				stream << "--------------------------------------\n" << stdnt[i]->getID() << " ";
				stream << stdnt[i]->getFIO() << "\n" << "Marks: ";
				stdnt[i]->showMarks(stream);
				stream << "Average mark: " << stdnt[i]->getAvgMark() << endl;
				stream << "--------------------------------------" << endl;
			}
			stream << "Headman:" << endl;
			if (!head)
				stream << "The headman hasn't been chosen" << endl;
			else
			stream << getHead()->getID() << " " << getHead()->getFIO() << endl;
			stream << "--------------------------------------\n" << "Average mark in the group: ";
			stream << getAvgMark() << "\n--------------------------------------" << endl;
			stream << "**************************************" << endl;
		}
	}
};
