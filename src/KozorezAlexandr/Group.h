#pragma once
#include "Student.h"

class Group
{
	private:
		string TITLE;
		Student* HEAD;
		vector<Student*> LIST;
	public:
		Group(string TITLE = "", Student* HEAD = NULL)
		{
			this->TITLE = TITLE;
			this->HEAD = HEAD;
		}
		~Group()
		{
			LIST.clear();
			HEAD = NULL;
			TITLE = "";
		}
		void setHead(Student* HEAD) { this->HEAD = HEAD; }
		Student* getHead() { return HEAD; }
		void setTitle(string TITLE) { this->TITLE = TITLE; }
		string getTitle() const { return TITLE; }
		double getAverage()
		{
			double avg = 0.0;
			int n = LIST.size();
			for (int i = 0;i < n;i++)
				avg += LIST[i]->getAverage();
			return (avg / n);
		}
		Student* find(int ID)
		{
			int n = LIST.size();
			for (int i = 0;i < n;i++)
				if (LIST[i]->getID() == ID) return LIST[i];
			return NULL;
		}

		void add(Student* ST) { LIST.Add(ST); }
		void remove(Student* ST)
		{
			int n = LIST.size();
			bool was = 0;
			for (int i = 0;i < n;i++)
				if (ST == LIST[i])
					LIST[i] = LIST.back(),
					LIST.resize(n - 1),
					was = 1;
				else if (was == 1) break;
		}
};