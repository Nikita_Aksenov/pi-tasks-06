#include <string>
#include <vector>
using namespace std;
class Group;
class Student {
private:
	int  id;
	string fio;
	Group* group;
	vector<int> marks;
public:
	Student(int id=0, string fio="", Group* group=NULL);
	void SetId(int id);
	void SetFio(string fio);
	void SetGroup(Group* group);
	int GetId();
	string GetFio();
	Group* GetGroup();
	void AddMark(int new_mark);
	double GetAvMark();
	~Student();
};

