#include "Dekanat.h"
Dekanat::Dekanat() {}

void Dekanat::LoadGroups() {
	ifstream fcin("Groups.txt");
	string title;
	while (fcin >> title)
		groups.push_back(new Group(title));
}

void Dekanat::LoadStud() {
	ifstream fscin("Students.txt");
	int id;
	string fio;
	string s1, s2, s3;
	string title;
	while (fscin >> id) {
		fscin >> s1>>s2>>s3;
		fio = s1 + " " + s2 + " "+s3;
		fscin >> title;
		for (int i = 0; i < groups.size(); ++i) {
			if (title == groups[i]->GetTitle()) {
				students.push_back(new Student(id, fio, groups[i]));
				groups[i]->AddStud(students.back());
			}
		}
	}
}

Student* Dekanat::FindStud(int id) {
	for (int i = 0; i < students.size(); ++i) {
		if (students[i]->GetId() == id)
			return students[i];
	}
	return NULL;
}

Group * Dekanat::FindGr(string title)
{
	for (int i = 0; i < groups.size(); ++i) {
		if (groups[i]->GetTitle() == title)
			return groups[i];
	}
	return NULL;
}

void Dekanat::RemoveStudGr(Student * stud) {
	Group* gr = stud->GetGroup();
	if (gr->GetHead() == stud) 
		gr->SetHead(NULL);
	stud->SetGroup(NULL);
	gr->RemoveStudent(stud);
}

void Dekanat::AddStudGr(Student* stud, Group* gr) {
	gr->AddStud(stud);
	stud->SetGroup(gr);
}

void Dekanat::TransferStud(int id, string title) {
	Student* stud = FindStud(id);
	Group* gr = FindGr(title);
	if (stud == NULL) {
		cout << "Student No"<<id<<" not found" << endl;;
		return;
	}
	if (gr == NULL) {
		cout << "Group No" << title << " not found" << endl;
		return;
	}
	RemoveStudGr(stud);
	AddStudGr(stud, gr);
	cout << "Student No" << id << " successfuly transfered in group No" << title << endl;
}

void Dekanat::AddStud(Student* new_stud, string title) {
	for (int i = 0; i < students.size(); i++) {
		if (students[i]->GetId() == new_stud->GetId()) {
			cout << "Id No" << new_stud->GetId() << " is bussy" << endl;
			return;
		}
	}
	Group* gr = FindGr(title);
	if (gr == NULL) {
		cout << "Group No" << title << " not found" << endl;
		return;
	}
	new_stud->SetGroup(gr);
	students.push_back(new_stud);
	gr->AddStud(new_stud);
	cout << "Student No" << new_stud->GetId() << " successfuly added in group No" << title << endl;
}

void Dekanat::RemoveStud(int id) {
	Student* stud = FindStud(id);
	if (stud == NULL) {
		cout << "Student No" << id << " not found" << endl;;
		return;
	}
	RemoveStudGr(stud);
	for (int i = 0; i < students.size(); ++i) {
		if (students[i] == stud) {
			students[i] = students.back();
			students.resize(students.size() - 1);
			break;
		}
	}
	cout << "Student No" << id << " successfuly remove" << endl;
}

void Dekanat::AddMark(int id, int mark) {
	FindStud(id)->AddMark(mark);
}

void Dekanat::AddHead(string title, int id) {
	Group* gr = FindGr(title);
	Student* stud = FindStud(id);
	if (stud == NULL) {
		cout << "Student No" << id << " not found" << endl;;
		return;
	}
	if (gr == NULL) {
		cout << "Group No" << title << " not found" << endl;
		return;
	}
	if (stud->GetGroup() != gr) {
		cout << "Group No" << title << " haven't student No" << id << endl;
		return;
	}
	gr->SetHead(stud);
	cout << "Head (student No" << id << ") successfuly added in group No" << title << endl;
}

void Dekanat::RandomMarks(int size) {
	for (int i = 0; i < students.size(); ++i) {
		for (int j = 0; j < size; ++j) {
			int new_mark = rand() % 5 + 1;
			AddMark(students[i]->GetId(), new_mark);
		}
	}
	cout << "Marks successfuly added" << endl;
}

Group* Dekanat::FindBestGroup() {
	Group* BestGr = groups[0];
	for (int i = 0; i < groups.size(); ++i) {
		if (BestGr->GetAvMark() < groups[i]->GetAvMark())
			BestGr = groups[i];
	}
	cout << "Best group is group No" << BestGr->GetTitle() << ". Averange mark is " << BestGr->GetAvMark() << endl;
	return BestGr;
}

Student* Dekanat::FindBestStudent() {
	Student* BestSt = students[0];
	for (int i = 0; i < students.size(); ++i) {
		if (BestSt->GetAvMark() < students[i]->GetAvMark())
			BestSt = students[i];
	}
	cout << "Best student is group No" << BestSt->GetId() << ". Averange mark is " << BestSt->GetAvMark() << endl;
	return BestSt;
}

void Dekanat::WriteStudents() {
	ofstream fout("NewStudents.txt");
	for (int i = 0; i < students.size(); ++i) {
		fout << students[i]->GetId() << " " << students[i]->GetFio() << " " << students[i]->GetGroup()->GetTitle() << endl;
	}
}

void Dekanat::WriteGroups() {
	ofstream fout("NewGroups.txt");
	for (int i = 0; i < groups.size(); ++i) {
		fout << groups[i]->GetTitle()<< endl;
	}
}

void Dekanat::Scenario() {
	TransferStud(60, "381303");
	TransferStud(4, "371605");
	TransferStud(1, "381608");;
	AddStud(new Student(1, "������ ��������� ����������"), "381608");
	AddStud(new Student(56, "������ ��������� ����������"), "381608-1");
	AddStud(new Student(56, "������ ��������� ����������"), "381608");
	RemoveStud(59);
	RemoveStud(4);
	AddHead("381608-1", 1);
	AddHead("381608", 60);
	AddHead("381603", 1);
	AddHead("381608", 1);
	RandomMarks(5);
	Group* BestGr = FindBestGroup();
	Student* BestSt = FindBestStudent();
}

Dekanat::~Dekanat() {
	for (int i = 0; i < groups.size(); ++i)
		delete[] groups[i];
	groups.clear();
	for (int i = 0; i < students.size(); ++i)
		delete[] students[i];
	students.clear();
}
