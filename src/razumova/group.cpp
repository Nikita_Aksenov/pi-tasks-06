#include "group.h"
#include <time.h>


using namespace std;


Group::Group(const Group &gr)
{
    course = gr.course;
    students = gr.students;
    head = gr.head;
    title = gr.title;
    course = gr.course;
}

Group::Group():course(1), title(""), head(NULL){}
Group::Group(string title, int course):head(NULL)
{
    this->title = title;
    this->course = course;
}

void Group::setStudents(vector<Student> s)
{
    if(s.size()>students.size())
    {
        for (int i =0; i<students.size();i++)
        {
            students[i] = &s[i];
        }
        for (int i = students.size(); i<s.size(); i++)
        {
            students.push_back(&s[i]);
        }
    }

}


void Group::addStudent(Student *st)
{
    students.push_back(st);
	students[students.size() - 1]->setGroup(this);
}

int Group::findStudent(Student &st)
{
    int res = -1;
    for(int i = 0; i<students.size(); i++ )
    {
        if (st.getId() == students[i]->getId())
        {
            res = i;
            return res;
        }
    }
    return res;
}

int Group::findStudent(int id)
{
    int res = -1;
    for(int i = 0; i<students.size(); i++ )
    {
        if (id == students[i]->getId())
        {
            res = i;
            return res;
        }
    }
    return res;
}

int Group::findStudent(string fio)
{
    int res = -1;
    for(int i = 0; i<students.size(); i++ )
    {
        if (fio == students[i]->getFio())
        {
            res = i;
            return res;
        }
    }
    return res;
}

void Group::removeStud(int ind)
{
    //Student *tmp = new Student [ Count-1];
	if (students[ind] == head)
	{
		//��������� ������ ��������, ���� ��� ���� �������
		changeHead();
	}
    if (ind >=0 && ind <students.size())
    {
        students[ind] = students[students.size()-1];
        students.pop_back();
    }

}

void Group::setHead(int ind)
{
    head = students[ind];
}

void Group::changeHead()
{
	int ind = findStudent(*head);
	if (ind == students.size() - 1)
	{
		setHead(0);
	}
	else
	{
		setHead(ind + 1);
	}
}

Student* Group::getHead()
{
    return head;
}

void Group::setMarks(int n)
{
	for (int i = 0; i < students.size(); i++)
	{
		for (int j = 1; j <= n; j++)
		{
			int mark = rand() % 4 + 2;//������ �� 2 �� 5
			students[i]->addMark(mark);
		}
	}
}

double Group::getAvMark()
{
    double avmark=0;
    for (int i=0; i<students.size(); i++)
    {
        avmark += students[i]->getAvMark();
    }
    avmark/= students.size();
    return avmark;
}


void Group::setTitle(string title)
{
    this->title = title;
}

string Group::getTitle()
{
	return title;
}

void Group::print()
{
    cout<<title<<endl;
    for (int i =0; i<students.size();i++)
    {
        students[i]->print();
    }

}
int Group::getDewager()
{
	int res = -1;
	for (int i = 0; i < students.size(); i++)
	{
		if (students[i]->getAvMark() < 2.5)
		{
			return students[i]->getId();
		}
	}
	return res;
}

int Group::getStudentsNum()
{

	return students.size();
}

Student * Group::bestStudent()
{
	double best = 0;
	Student * res = 0;
	for (int i = 0; i < students.size(); i++)
	{
		if (students[i]->getAvMark() >= best)
		{
			best = students[i]->getAvMark();
			res = students[i];
		}
	}
	return res;
}

