#include <string>
#include <iostream>

using namespace std;

class Group;

class Student
{
private:
	int ID;
	string FIO;
	Group * gr;
	double * Marks;
	int Num;
public:
	Student(int ID, string FIO)
	{
		this->ID = ID;
		this->FIO = FIO;
		Num = 0;
		Marks = new double[Num];		
	}
	void setGroup(Group * gr);
	void addMark(double Mark);
	double getAverageRating();
	string getFIO();
	int getID();
	Group * getGroup();
	~Student()
	{
		delete[]Marks;
	}
};