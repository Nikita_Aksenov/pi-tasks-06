#include "Student.h"
#include "Group.h"

string Group::getTitle()
{
	return Title;
}

void Group::addStudent(Student * st)
{
	Student ** tmp=new Student * [Num+1];
	for (int i = 0; i < Num; i++)
	{
		tmp[i] = Students[i];
	}
	delete[] Students;
	Students = tmp;
	Students[Num] = st;
	Num++;
}

void Group::setHead(Student * st)
{
	Head = st;
}

Student * Group::searchStud(string FIO)
{
	for (int i = 0; i < Num; i++)
	{
		if (Students[i]->getFIO() == FIO)
			return Students[i];
	}
	return nullptr;
}

Student * Group::searchStud(int ID)
{
	for (int i = 0; i < Num; i++)
	{
		if (Students[i]->getID() == ID)
			return Students[i];
	}
	return nullptr;
}

double Group::getAverageRating()
{
	double AverageRating = 0;
	double MarksSum = 0;
	for (int i = 0; i < Num; i++)
	{
		MarksSum += Students[i]->getAverageRating();
	}
	AverageRating = MarksSum / Num;
	return AverageRating;
}

void Group::excludeStud(Student * st)
{
	int st_num;
	for (int i = 0; i < Num; i++)
	{
		if (Students[i] == st)
		{
			st_num = i;
			break;
		}
	}
	Students[st_num] = Students[Num-1];
	
	Student ** tmp = new Student *[--Num];
	for (int i = 0; i < Num; i++)
	{
		tmp[i] = Students[i];
	}
	delete[] Students;
	Students = tmp;
}

int Group::getNum()
{
	return Num;
}

Student * Group::getStud(int Index)
{
	return Students[Index];
}

Student * Group::getHead()
{
	return Head;
}